# Speech Recognition CLI - MacOS Command Line

## Introduction

Speech Recognition CLI is MacOS command lind tool that translate Mac's supported audio file into text directly from your Mac.

Speech Recognition CLI use Mac's on-device speech recognition. It support languages as much as your Mac support. You can see list of your Mac's support languages with command

```
srData -s
```
## Installation

Using Speech Recognition CLI, simply run the project, terminal will show up with where the build CLI file create.

![](https://gitlab.com/kaiapp/speech-recognition-macos-command-line/-/raw/main/assets/srData_command_screen.png)

If you want to use Speech Recognition CLI on terminal, just simply copy srData file to /usr/local/bin/

## How to use

```
Usage: srData [-i input] [-l language] [--not_on_device]

Options:

  -i --input [file_path]  Specify audio file to process
  -l --language           Specify speech recognition language
  --not_on_device         Not use on-device speech recognition


  -h --help               Prints help
  -s --supported          Print list of supported languages
```

### Example

Audio file -> text in Thai
```
srData -i path/to/voice.wav -l th-TH
```

Audio file -> text in Japanese use speech recognition from Apple server (online).
```
srData -i path/to/voice.wav -l ja-JP --not_on_device
```

The result would be like this
```
{"fileName":"srData_sample.wav","words":["","This","is","speech","recognition","sample"],"ends":[0.0700,0.3300,0.4800,0.8700,1.5000,2.1400]}
```

I use this CLI for speech syncthesizer training.
