//
//  SRData.swift
//  srData
//
//  Created by Thosapol Sangkasub on 17/2/23.
//

import Foundation
import Speech
import AVFoundation

class SRData {
    
    let consoleIO = ConsoleIO()

    func staticMode() {
        if SFSpeechRecognizer.authorizationStatus() == .authorized {
            doASR()
        } else {
            askSpeechpermit()
        }
    }
    
    func doASR() {
        var command: String = "asr"
        var input_str: String = ""
        var lang: String = "th"
        var onDevice: Bool = true
        
        var i: Int = 0
        var option: String = ""
        for argument in CommandLine.arguments {
            if i == 0 && CommandLine.argc == 1 {
                    command = "help"
            } else if i == 1 {
                if argument == "-i" || argument == "--input" {
                    command = "asr"
                } else if argument == "-h" || argument == "--help" {
                    command = "help"
                } else if argument == "-s" || argument == "--supported" {
                    command = "supported"
                } else {
                    command = "unknown"
                }
            } else {
                if command == "asr" && i == 2 {
                    input_str = argument
                } else if argument == "-l" || argument == "--language" {
                    option = "language"
                } else if option == "language" {
                    lang = argument
                    option = ""
                } else if argument == "--not_on_device" {
                    onDevice = false
                    option = ""
                }
            }
            i += 1
        }
        if command == "help" {
            consoleIO.printHelp()
        } else if command == "supported" {
            printSupportedLanguages()
        } else if command == "asr" {
            if !input_str.isEmpty {
                if let fileurl: URL = URL(string: input_str) {
                    audioToText(file: fileurl, lang: lang, onDevice: onDevice)
                } else {
                    consoleIO.writeMessage("Audio file location path incorrect", to: .error)
                    exit(EXIT_SUCCESS)
                }
            } else {
                consoleIO.writeMessage("Please provide audio file location after -i ", to: .error)
                exit(EXIT_SUCCESS)
            }
        } else {
            consoleIO.writeMessage("Unknown option", to: .error)
            exit(EXIT_SUCCESS)
        }
    }
    
    func audioToText(file: URL, lang: String, onDevice: Bool) {
        if let recognizer = SFSpeechRecognizer(locale: Locale.init(identifier: lang)) {
            if recognizer.isAvailable {
                var i = 0
                let request = SFSpeechAudioBufferRecognitionRequest()
                recognizer.supportsOnDeviceRecognition = onDevice
                
                do {
                    let audioFile = try AVAudioFile(forReading: file)
                    let audioFormat = audioFile.processingFormat
                    guard let audioFileBuffer = AVAudioPCMBuffer(pcmFormat: audioFormat, frameCapacity:AVAudioFrameCount(audioFile.length)) else{
                        return }
                    try audioFile.read(into: audioFileBuffer)
                    request.append(audioFileBuffer)
                    request.endAudio()
                } catch {
                    self.consoleIO.writeMessage(error.localizedDescription, to: .error)
                    exit(EXIT_SUCCESS)
                }
                recognizer.recognitionTask(with: request) { result, error in
                    if let error = error {
                        // handle error
                        self.consoleIO.writeMessage(error.localizedDescription, to: .error)
                        exit(EXIT_SUCCESS)
                    } else if let result = result {
                        if result.isFinal {
                            var i = 0
                            var words = Array<String>()
                            var ends = Array<String>()
                            var last_end: Float = 0
                            for itm in result.bestTranscription.segments {
                                if i == 0 && itm.timestamp > 0 {
                                    words.append("")
                                    ends.append(String(itm.timestamp))
                                    last_end = Float(itm.timestamp)
                                }
                                if last_end < Float(itm.timestamp) {
                                    words.append("")
                                    ends.append(String(itm.timestamp))
                                }
                                words.append(itm.substring)
                                ends.append(String(itm.timestamp + itm.duration))
                                last_end = Float(itm.timestamp + itm.duration)
                                i += 1
                            }
                            let fileName = file.lastPathComponent
                            var word2 = Array<String>()
                            for word in words {
                                word2.append("\"" + word.replacingOccurrences(of: " ", with: "") + "\"")
                            }
                            var ends2 = Array<String>()
                            for end in ends {
                                let end2 = Float(end)
                                ends2.append(String(format: "%.4f", end2!))
                            }
                            let outstr = "{\"fileName\":\"" + fileName + "\",\"words\":[" + word2.joined(separator: ",") + "],\"ends\":[" + ends2.joined(separator: ",") + "]}"
                            self.consoleIO.writeMessage(outstr)
                            exit(EXIT_SUCCESS)
                        }
                    }
                    i += 1
                }
            } else {
                askSpeechpermit()
            }
        }
    }
    
    func askSpeechpermit() {
        SFSpeechRecognizer.requestAuthorization { (status) in
            switch status {
            case .notDetermined: print("Not determined")
                exit(EXIT_SUCCESS)
            case .restricted: print("Restricted")
                exit(EXIT_SUCCESS)
            case .denied: print("Denied")
                exit(EXIT_SUCCESS)
            case .authorized: print("We can recognize speech now.")
                self.staticMode()
            @unknown default: print("Unknown case")
                exit(EXIT_SUCCESS)
          }
        }
    }
    
    func printSupportedLanguages() {
        let enUS = Locale(identifier: "en-US")
        for locale in SFSpeechRecognizer.supportedLocales() {
            let localename: String = enUS.localizedString(forLanguageCode: locale.identifier).unsafelyUnwrapped
            consoleIO.writeMessage("\(locale.identifier)        \(localename)")
        }
        exit(EXIT_SUCCESS)
    }
}
