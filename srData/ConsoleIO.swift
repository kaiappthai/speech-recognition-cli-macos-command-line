//
//  ConsoleIO.swift
//  srData
//
//  Created by Thosapol Sangkasub on 17/2/23.
//

import Foundation

class ConsoleIO {
    let version: String = "1.0"
    let author: String = "Thosapol Sangkasub"
    
    enum OutputType {
        case error
        case standard
    }
    
    func writeMessage(_ message: String, to: OutputType = .standard) {
      switch to {
      case .standard:
        print("\(message)")
      case .error:
        fputs("Error: \(message)\n", stderr)
      }
    }
    
    func printHelp() {
        let executableName: String = (CommandLine.arguments[0] as NSString).lastPathComponent
        
        writeMessage("Usage: \(executableName) [-i input] [-l language] [--not_on_device]")
        writeMessage("")
        writeMessage("Options:")
        writeMessage("")
        writeMessage("  -i --input [file_path]  Specify audio file to process")
        writeMessage("  -l --language           Specify speech recognition language")
        writeMessage("  --not_on_device         Not use on-device speech recognition")
        writeMessage("")
        writeMessage("")
        writeMessage("  -h --help               Prints help")
        writeMessage("  -s --supported          Print list of supported languages")
        writeMessage("")
        writeMessage("\(executableName) version \(version) by \(author)")
        exit(EXIT_SUCCESS)
    }
}
